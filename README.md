# FGAU-Aus

========= FlightGear Australia =========
====== Australian Airports Package =====

<= ABOUT THIS PACKAGE =>
FlightGear Australia proudly presents the Australian Airports Package.  The focus of this package and project is to populate as many Australian airports, big or small, with scenery objects - with a desire of bringing life to this beautiful country within the free and open source flight simulator, FlightGear.  This package works best alongside the Australian Regional Materials and Australian Scenery Package also available from fgau.org (with thanks to frtps!)

<= INSTALLATION =>
To install, rename fgau-aus-master as FGAU-Aus, then place the FGAU-Aus folder into your FlightGear/Custom Scenery directory.  Then, add this directory as an addon within the FlightGear launcher, or point to it via command line options.

<= POPULATED AIRPORTS =>
As of 20/02/2020:
YGFN - Grafton
YSGR - South Grafton
YSSY - Sydney Kingsford Smith

<= CREDITS =>
Figaro (Fig),
John Ross,
Scotth1,
Zbyszek

<= WITH THANKS TO =>
frtps - for his work on Australian terrain,
merspieler - for his OSM2City for Sydney

<= CONTACT =>
contact@fgau.org
